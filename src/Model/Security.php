<?php


namespace App\Model;


use App\Repository\AttributeRepository;
use App\Repository\FactsRepository;
use App\Repository\SecurityRepository;

class Security
{
    private string $symbol = '';
    private ?int $id = 0;
    private array $facts = [];

    private AttributeRepository $attributeRepository;

    public function __construct(string $symbol)
    {
        $securityRepository = new SecurityRepository();
        $this->id = $securityRepository->findByName($symbol);
        if (empty($this->id)) {
            throw new \UnexpectedValueException('Unable to find Security: ' . $symbol);
        }
        $this->symbol = $symbol;

        // we can lookup by ID as we need to check facts
        $this->attributeRepository = new AttributeRepository();

        $factsRepository = new FactsRepository();
        $this->facts = $factsRepository->loadBySecurityId($this->id);
    }

    public function getFactByName(string $name): float
    {
        $factId = $this->attributeRepository->findByName($name);
        return $this->facts[$factId];
    }


}
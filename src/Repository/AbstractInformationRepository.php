<?php


namespace App\Repository;


abstract class AbstractInformationRepository
{
    // Quick way of faking an inverted index on a simple data structure
    protected array $dataById = [];
    protected array $dataByName = [];
    protected string $repositoryLocation = '';

    protected function loadAll(): void
    {
        $dataSource = fopen(dirname(dirname(dirname(__FILE__))) . '/' . $this->repositoryLocation, 'r');
        $csvIndex = 0;
        while (($csv = fgetcsv($dataSource)) !== false) {
            if ($csvIndex > 0) {
                // skip the header
                $this->dataById[$csv[0]] = $csv[1];
                $this->dataByName[$csv[1]] = $csv[0];
            }
            $csvIndex++;
        }
    }

    /**
     * Returns null if not found
     * @param int $id
     * @return string|null
     */
    public function findById(int $id): ?string
    {
        if (!array_key_exists($id, $this->dataById)) {
            return null;
        }
        return $this->dataById[$id];
    }

    /**
     * Find the ID of a given security. Returns Null if not found
     * @param string $name
     * @return int|null
     */
    public function findByName(string $name): ?int
    {
        if (!array_key_exists($name, $this->dataByName)) {
            return null;
        }
        return $this->dataByName[$name];
    }
}
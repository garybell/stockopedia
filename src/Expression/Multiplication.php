<?php


namespace App\Expression;


class Multiplication implements ExpressionInterface
{

    function evaluate(float $a, float $b): ?float
    {
        return $a * $b;
    }
}
<?php


namespace App\Expression;


class ExpressionFactory
{
    public static function newExpressionEvaluator(string $symbol): ExpressionInterface
    {
        switch($symbol) {
            case '+':
                return new Addition();
                break;
            case '-':
                return new Subtraction();
                break;
            case '/':
                return new Division();
                break;
            case '*':
                return new Multiplication();
            default:
                throw new \UnexpectedValueException("The evaluator symbol '" . $symbol . " was not recognised");
        }
    }
}
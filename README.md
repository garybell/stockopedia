# Stockopedia tech test

## Installation
Clone the repository and run `composer install` from the root directory. This should install all required components

The installation will include the apache-pack to allow the URLs to not require `index.php` when running under apache. 
If this is running under Apache HTTP server, the `AllowOverrides All` directive needs to be set for the folder.

Data CSV files need to be added to the `var/data` folder, and follow the naming supplied with the task. They need to be readable.

The full `var` structure may need creating on checkout. This currently look slike:
```
var/
  /cache
  /data
  /log
```
The cache and log folders need to be writable. The data folder just needs read access once the data files are there.

## Using the application
The request URL to call the application is `<host>/analytics/` e.g. `http://localhost/analytics/`. This requires the query to be POSTed to the server.

Any non-POST requests will return a 405 - Method not allowed response.

## Design Decisions

### Responses
As no structure was given for what the response should look like, I decided to return the result alongside the required expressions and the security. 
Doing so allows the recipient of the response to know exactly what the response refers to, should they log the full response on return.

Where errors are returned, this is done with a JSON error message, and a HTTP status code to reflect the issue.

### Repositories
As these are the same schema and only differ in filename, the entire functionality for populating and searching can be abstracted out. 
As such, these 'information' tables both extend the `AbstractInformationRepository` and only differ in by `$repositoryLocation` to keep things DRY.

The FactsRepository cannot extend the AbstractInformationRepository because `findById` could refer to either the Security ID or the Attribute ID

### Expressions
Each expression implements the ExpressionInterface. This only has one function (`evaluate($a, $b)`) but means the application can extend to include other functionality. 
For example, the symbol `^` could be exponent. This could be created with the following:

```php
namespace App\Expression;
class Exponent implements ExpressionInterface
{
    function evaluate(float $a, float $b): ?float
    {
        return $a ** $b;
    }
}
```

And then adding the relevant builder into the Expression factory.

A choice has been made here to return 0 if `$b` is 0 within Division rather than get a DivideByZeroException, or fatal error. 
The alternative is the application crashes, or return the highest possible number as an approximation to infinity. 
Neither of those alternatives are idea.

## ToDo:
1. Add unit tests. 
   This has to be the absolute priority as next steps for this to ensure it is functionally correct.
1. Re-evaluate Division and error handling. 
   Returning 0 when dividing by zero isn't ideal. Instead it should return an error code which reflects the issue. 
   This would need further exploration as 400 isn't fair to the user (they cannot reasonably be expected to know every fact which has value 0 to avoid dividing by). 
   It could be argued that 409 (conflict) is appropriate, but alternatively a custom code could be used.
   
## Things for further consideration:
* Request security/rate limiting - there is no consideration for security around the URL, nor any rate limiting
* Custom exceptions - this could provide better information to the end user, and more useful end-user documentation
* Documentation - this file is the only documentation for the application. Refining the code comments and adding Swagger could help here.
   
## Time Spent
As I've not added this to Git prior to submission, it's difficult to prove the time spend on this. 
I'd estimate that, including this documentation, the total time spent so far is around 4 hours.

I anticipate a further 90-120 minutes required for unit testing, and then around 15 minutes to rework the Division to return an appropriate error and unit test that.
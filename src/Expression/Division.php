<?php


namespace App\Expression;


class Division implements ExpressionInterface
{

    function evaluate(float $a, float $b): ?float
    {
        if ($b == 0) {
            return 0;
        }
        return $a / $b;
    }
}
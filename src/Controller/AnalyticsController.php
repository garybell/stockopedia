<?php


namespace App\Controller;


use App\Expression\ExpressionFactory;
use App\Model\Security;
use App\Repository\SecurityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AnalyticsController
 * @package App\Controller
 * @Route("/analytics", name="analytics_")
 */
class AnalyticsController extends AbstractController
{

    /**
     * @Route("/", methods={"POST"}, name="analytics_index")
     * @param Request $request
     * @return Response
     */
    public function analyse(Request $request): Response
    {
        $requestBody = $request->getContent();
        $requestArray = json_decode($requestBody, true);
        if (!$this->validRequest($request)) {
            return new JsonResponse(['error' => 'Invalid request body'], 401);
        }
        try {
            $security = new Security($requestArray['security']);
        } catch (\UnexpectedValueException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 404);
        }

        $result = $this->evaluateRequest($security, $requestArray['expression']);

        return new JsonResponse([
            'result' => $result,
            'security' => $requestArray['security'],
            'expression' => $requestArray['expression']
        ]);
    }

    /**
     * Make sure the request is valid before processing
     * @param Request $request
     * @return bool
     */
    private function validRequest(Request $request): bool
    {
        $decoded = json_decode($request->getContent(), true);
        if (empty($decoded) ||
            !array_key_exists('security', $decoded) ||
            !array_key_exists('expression', $decoded)) {
            return false;
        }
        return true;
    }

    private function evaluateRequest(Security $security, array $expression): float
    {
        if (is_array($expression['a'])) {
            $a = $this->evaluateRequest($security, $expression['a']);
        } else {
            $a = $security->getFactByName($expression['a']);
        }

        if (is_array($expression['b'])) {
            $b = $this->evaluateRequest($security, $expression['b']);
        } else {
            $b = $security->getFactByName($expression['b']);
        }
        $evaluator = ExpressionFactory::newExpressionEvaluator($expression['fn']);

        return $evaluator->evaluate($a, $b);
    }
}
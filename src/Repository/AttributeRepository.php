<?php


namespace App\Repository;


class AttributeRepository extends AbstractInformationRepository
{
// Quick way of faking an inverted index on a simple data structure
    protected array $dataById = [];
    protected array $dataByName = [];
    protected string $repositoryLocation = 'var/data/attributes.csv';

    public function __construct()
    {
        $this->loadAll();
    }
}
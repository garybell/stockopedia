<?php


namespace App\Repository;


class SecurityRepository extends AbstractInformationRepository
{
    // Quick way of faking an inverted index on a simple data structure
    protected array $dataById = [];
    protected array $dataByName = [];
    protected string $repositoryLocation = 'var/data/securities.csv';

    public function __construct()
    {
        $this->loadAll();
    }
}
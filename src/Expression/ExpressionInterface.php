<?php


namespace App\Expression;


interface ExpressionInterface
{
    function evaluate(float $a, float $b): ?float;
}
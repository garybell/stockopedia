<?php


namespace App\Expression;


class Addition extends AbstractExpression
{
    function evaluate(float $a, float $b): ?float
    {
        return $a + $b;
    }
}
<?php


namespace App\Repository;


class FactsRepository
{
    private string $repositoryLocation = 'var/data/facts.csv';

    public function loadBySecurityId(int $securityId): array
    {
        $facts = [];

        $dataSource = fopen(dirname(dirname(dirname(__FILE__))) . '/' . $this->repositoryLocation, 'r');
        $csvIndex = 0;
        while (($csv = fgetcsv($dataSource)) !== false) {
            if ($csvIndex > 0 && $csv[0] == $securityId) {
                // skip the header, and limit by security ID
                $facts[$csv[1]] = $csv[2];
            }
            $csvIndex++;
        }

        return $facts;
    }
}
<?php


namespace App\Expression;


class Subtraction implements ExpressionInterface
{

    function evaluate(float $a, float $b): ?float
    {
        return $a - $b;
    }
}